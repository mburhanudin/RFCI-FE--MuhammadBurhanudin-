<html>
<head>
	<title>Test</title>
	<style>
		body {
			font-family: "segoe ui";
		}
		h1 {
			font-size: 25px;
		}
		input, select {
			border: 1px solid #CCCCCC;
			padding: 7px;
			font-size: 14px;
		}
		input[type="submit"] {
			padding: 7px 15px;
			margin-left: 120px;
			cursor: pointer;
		}
		label {
			width: 120px;
			display: block;
			float: left;
		}
		.checkbox, .radio {
			float:none;
			width: auto;
		}
		.row::after {
			content: "";
			display: block;
			clear:both;
		}
		.row {
			margin-bottom: 5px;
			clear: both;
		}
		.options {
			float:left;
		}
	</style>
</head>
<body>
	<h1>Contoh</h1>
	<form action="" method="post">
		<div class="row">
			<label></label>
			<input id="hasil" type="text" name="nama" value="<?=isset($_POST['nama']) ? $_POST['nama'] : ''?>"/>
		</div>
		
        <?php
	if (isset($_POST['submit'])) {
		echo '<h1>Output</h1>';
		echo '<ul>';
		echo '<li>' . $_POST['nama'] . '</li>';
		
	}?>
		<div class="row">
        <input type="button" onclick="reverse()" value="Reverse">
			
            <input type="submit" name="submit" value="Undo/Redo"/>
		</div>
	</form>
	<script type="text/javascript">
     function reverse(){
           x=document.getElementById('teks').value; //mengambil tulisan yang akan direverse
           var hsl,p;
           hsl=""; //menginisialisasi variabel hsl dengan tanda kutip. Ini berfungsi agar ketika digunakan variabel ini sidah ada isinya
           p=x.length; //mengambil panjang dari tulisan yang akan direfvese
           for(a=p-1;a>=0;a--){ //perulangan dimulai dari p-1 karena tulisan sring sama seperti array of char. Dimulai dari index ke 0 sehingga jika panjannya n maka tulisan berakhir di n-1
                hsl=hsl+x.charAt(a); //mengambil tulisan di index ke a lalu menampungnya di variable hsl
           }
           document.getElementById('hasil').value=hsl; //proses menampilkan
     }
</script>
</body>
</html>